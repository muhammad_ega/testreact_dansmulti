import React, { Component } from 'react';

import './App.scss';
import { GoogleLogin } from './Login/Login'; 
 
import ListPage from './Page/ListPage';
import DetailPage from './Page/DetailPage';
import { Routes, Route, redirect  } from 'react-router-dom';

class App extends Component{
  constructor(){
    super();
    
    this.state = {
      token: localStorage.getItem('x-tokenID')
    } 
    // this.logOut = this.logOut.bind(this);
  }
  componentDidMount(){
    
    // if(this.state.token !== null){
      return redirect("/list");
    // }
  }
  componentDidUpdate(prevState){
    if(this.state.token !== null){
      return redirect("/list");
    }
  }
  render(){
    if(this.state.token !== null){
       return(
        
        <>
        
        <GoogleLogin/>
       
           <Routes>
                <Route path='/list' element={( <ListPage/>)} />
                <Route  path='/detail/:id' element={( <DetailPage/>)} />
            </Routes> 
       </>
       )
    }else{
      return (
        <>
            <GoogleLogin/>
        </>
      );
    }
  
}
}

export default App;