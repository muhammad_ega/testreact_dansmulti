import { render, screen } from '@testing-library/react';
import DetailPage from './DetailPage';

test('renders learn react link', () => {
  render(<DetailPage />);
  const linkElement = screen.getByText(/learn react/i);
  expect(linkElement).toBeInTheDocument();
});
