import React, { useRef, useEffect, useState } from "react";
import ListigPageComponent from "../../components/ListingPage";

import axios from "axios";

function ListingPageContainer(props) {
  const listInnerRef = useRef();
  const [currPage, setCurrPage] = useState(1);
  const [prevPage, setPrevPage] = useState(0);
  const [DataList, setDataList] = useState([]);
  const [lastList, setLastList] = useState(false);

  useEffect(() => {
    const fetchData = async () => {
      const response = await axios.get( 
        `http://dev3.dansmultipro.co.id/api/recruitment/positions.json?page=${currPage}`
      );
      // console.log(response.data, "<<<");
      if (!response.data.length) {
         setLastList(true);
        return;
      }
    setPrevPage(currPage);
             setDataList([...DataList, ...response.data]);
    };
    if (!lastList && prevPage !== currPage) {
      fetchData();
    }
  }, [currPage, lastList, prevPage, DataList]);

  const onScroll = () => {
    if (listInnerRef.current) {
      const { scrollTop, scrollHeight, clientHeight } = listInnerRef.current;
      if (scrollTop + clientHeight === scrollHeight) {
        setCurrPage(currPage + 1);
      }
    }
  };

  return (
    <ListigPageComponent
      onScroll={onScroll}
      listInnerRef={listInnerRef}
      DataList={DataList}
    />
  );
}

export default ListingPageContainer;
