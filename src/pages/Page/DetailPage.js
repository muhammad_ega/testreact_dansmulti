import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import axios from "axios";
 

class DetailPage extends Component {

    constructor() {
        super();
        this.state = {
            // signIn: ''
            company: null,
            data: null,
            isLoaded :false,
        }
    }
   async fetchUrl() {
        let  IdParam = ""
           IdParam = window.location.href.split('/')[4]
     await axios.get('http://dev3.dansmultipro.co.id/api/recruitment/positions/' + IdParam)
            
            .then((resp) => {
                this.setState({
                    company: resp.data.company,
                    created_at: resp.data.created_at,
                    logo: resp.data.company_logo,
                    description: resp.data.description,
                    how_to_apply: resp.data.how_to_apply,
                    location: resp.data.location,
                    title: resp.data.title,
                    type: resp.data.type,
                    url: resp.data.url,
                    isLoaded: true,
                })

            });
    }
    componentDidMount() {
        this.fetchUrl();
    }

    componentDidUpdate(prevProps, prevState) { }

    componentWillUnmount() { }



    render() {

        return (
            <>
                <div className='container DetailPage'>
                    <h2 className='title text-upper'>
                    
                        <Link to="/list" className='backbtn'> &#8592; </Link>
                      {this.state.title}</h2>
                    <h6 className='sub-title'>{this.state.type} | <span>{this.state.location}</span></h6>
                    <div className='con-1'> 
                    
                        <div className='w-80'>
                        <React.Fragment>
                            {!this.state.isLoaded &&
                            <div className="text-info">
                                Please wait...
                            </div>
                            }

                            {this.state.isLoaded &&
                            <div id="terms-content" dangerouslySetInnerHTML={{__html: this.state.description}}/>
                            }

                        </React.Fragment>
                        </div>
                        <div className='w-20'>
                            <h2 className='title'>How to Apply</h2>
                            <div className='content'>
                            <React.Fragment>
                            {!this.state.isLoaded &&
                            <div className="text-info">
                                Please wait...
                            </div>
                            }

                            {this.state.isLoaded &&
                            <div id="terms-content" dangerouslySetInnerHTML={{__html: this.state.how_to_apply}}/>
                            }

                        </React.Fragment>
                            </div>
                        </div>
                    </div>
                </div>
            </>
        );
    }
}

export default DetailPage;