import React from 'react';
import { redirect } from 'react-router-dom';
export const UserProfile = (props) => {
   
    redirect("/list");
   
 
  return (
    <div className='profile'>
      <h2>{props.user.name}</h2>
      <img src={props.user.profileImg} alt="user profile" />
    </div>
  );
}