import React, { useState, useEffect } from 'react';
import { gapi, loadAuth2 } from 'gapi-script'

import { UserProfile } from './UserProfile';
import { Link, redirect } from 'react-router-dom';

export const GoogleLogin  = () => {
  const [user, setUser] = useState(null);
 
  useEffect(() => {
    const setAuth2 = async () => {
      const auth2 = await loadAuth2(gapi, '386932037035-k8v833noqjk7m4auae0t83vnkrqvvg3t.apps.googleusercontent.com', '')
      if (auth2.isSignedIn.get()) {
          updateUser(auth2.currentUser.get())
          // console.log('berhasil')
          
          
      } else {
          attachSignin(document.getElementById('customBtn'), auth2);
      }
    }
    setAuth2();
  }, []);

  useEffect(() => {
    if (!user) {
      const setAuth2 = async () => {
        const auth2 = await loadAuth2(gapi, '386932037035-k8v833noqjk7m4auae0t83vnkrqvvg3t.apps.googleusercontent.com', '')
        attachSignin(document.getElementById('customBtn'), auth2);
      } 
      setAuth2();
    } 
  }, [user])

  const updateUser = (currentUser) => {
    const name = currentUser.getBasicProfile().getName();
    const profileImg = currentUser.getBasicProfile().getImageUrl();
   let tokenID = currentUser.xc.access_token
    localStorage.setItem("x-tokenID", tokenID);
    setUser({
      name: name,
      profileImg: profileImg,
    });
  };

  const attachSignin = (element, auth2) => {
    auth2.attachClickHandler(element, {},
      (googleUser) => {
        updateUser(googleUser);
        setTimeout(() => {
          window.location.reload();
        }, 500);
      
      }, (error) => {
      
      console.log(JSON.stringify(error))
    });
  };

  const signOut = () => {
    const auth2 = gapi.auth2.getAuthInstance();
    auth2.signOut().then(() => {
      setUser(null);
      localStorage.removeItem('x-tokenID')
      setTimeout(() => {
        document.cookie.split(";").forEach(function(c) { document.cookie = c.replace(/^ +/, "").replace(/=.*/, "=;expires=" + new Date().toUTCString() + ";path=/"); });
      window.location.reload();
    }, 500);
      // console.log('User signed out.');
    });
  }

  if(user) {
    redirect("/list");
    return (
      <div className="header-login">
        <UserProfile user={user} />
        <div id="" className="btn-logout" onClick={signOut}>
         <Link to="/">Logout</Link>
        </div>
      </div>
    );
  }

  return (
    <>

    <div className="for-Login">
    <h4  >Hi bro Please Login for See <br/> List Job</h4>
      <div id="customBtn" className="btn-login">
        
        <Link to="/list">Login With Google</Link>
      </div>
    </div>
    </>
  );
}