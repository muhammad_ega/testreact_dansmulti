import { React } from "react";
import * as moment from 'moment';
import { Link } from 'react-router-dom';
import PlaceHolder from "../assets/img/placeholder.jpg"
function ListingPageComponent({ onScroll, listInnerRef, DataList }) {
  const DataListfilter = DataList.filter(elements => {
    return elements !== null;
   });
  return (
    <div className='container list-page'>
       <h2>Job List</h2>
      <ul
        onScroll={onScroll}
        ref={listInnerRef} 
      >
        {DataListfilter.map((item, index) => {
          console.log(item,'<>L')
          return (
            <li className='card-list' key={index.id ? index.id : ''}>
               <Link to={`/detail/${item.id}`} params={{id: index.id}}>
                            <img src={PlaceHolder} alt="" />
                            <div className='  content-1'>
                                <h4 className='title'>{item.title}</h4>
                            <h6 className='sub-title'> Company : {item.company}</h6>
                            <h6 className='type'>{item.type}</h6> 
                            </div>
                            <div className='content-2'>
                                <h6 className='location'>{item.location}</h6>
                                <h5 className='day'> {moment(item.created_at).format('D MMMM YYYY • HH:mm:ss')}</h5>
                            </div>
                            </Link>
            </li>
          );
        })}
      </ul>
      </div>
    
  );
}

export default ListingPageComponent;
