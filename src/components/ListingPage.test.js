import { render, screen } from '@testing-library/react';
import ListingPage from './ListingPage';

test('renders learn react link', () => {
  render(<ListingPage />);
  const linkElement = screen.getByText(/learn react/i);
  expect(linkElement).toBeInTheDocument();
});
